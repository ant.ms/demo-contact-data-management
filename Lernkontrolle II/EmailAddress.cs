﻿namespace Lernkontrolle_II {
    class EmailAddress : ContactData {
        public string Email;

        public EmailAddress(string email) {
            Email = email;
        }

        /// <summary>
        /// Adds data to the Print function that's already defined
        /// </summary>
        public override void Print() {
            //base.Print();
            System.Console.WriteLine("Email: " + Email);
        }
    }
}