﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.CompilerServices;

namespace Lernkontrolle_II {
    class Person {

        public string FirstName;
        public string LastName;
        public DateTime Birthday;
        public Gender Gender;

        private List<ContactData> contactDatas = new List<ContactData>();

        // get all variables through the constructor
        public Person(string firstName, string lastName, DateTime birthday, Gender gender = Gender.Other) {
            FirstName = firstName;
            LastName = lastName;
            Birthday = birthday;
            Gender = gender;
        }

        public ContactData AddContactData(ContactData data) {
            contactDatas.Add(data);
            return data;
        }
        public ContactData RemoveContactData(ContactData data) {
            contactDatas.Remove(data);
            return data;
        }

        public PhoneNumber SetPrimaryPhoneNumber(PhoneNumber primary) {
            try {
                foreach (PhoneNumber element in contactDatas.OfType<PhoneNumber>()) {
                    if (primary.Number == element.Number) {
                        contactDatas.Remove(element);
                        contactDatas.Insert(0, element);
                        return primary;
                    }
                }
            } catch { }
            Console.WriteLine("Error, no matching Phone Number found");
            return primary;
        }
        public EmailAddress SetPrimaryEmailAddress(EmailAddress primary) {
            try {
                foreach (EmailAddress element in contactDatas.OfType<EmailAddress>()) {
                    if (primary.Email == element.Email) {
                        contactDatas.Remove(element);
                        contactDatas.Insert(0, element);
                        return primary;
                    }
                }
            } catch { }
            Console.WriteLine("Error, no matching Email found");
            return primary;
        }
        public PostalAddress SetPrimaryPostalAddress(PostalAddress primary) {
            try {
                foreach (PostalAddress element in contactDatas.OfType<PostalAddress>()) {
                    if (primary.Street == element.Street) {
                        contactDatas.Remove(element);
                        contactDatas.Insert(0, element);
                        return primary;
                    }
                }
            } catch { }
            Console.WriteLine("Error, no matching Postal Address found ");
            return primary;
        }

        /// <summary>
        /// This function outputs everything
        /// </summary>
        protected virtual void Print() {
            Console.WriteLine("FirstName: " + FirstName);
            Console.WriteLine("LastName: " + LastName);
            Console.WriteLine("Birthday: " + Birthday.ToLongDateString());
            Console.WriteLine("Gender: " + Gender);

            // this could be enabled if, instead of only the primary ones, you would want to output everyone
            //foreach (ContactData contactData in contactDatas) {
            //    contactData.Print();
            //}

            // print the first (primary) entry of the specific type that can be found
            try {
                contactDatas.OfType<EmailAddress>().First().Print();
            } catch  { }
            try {
                contactDatas.OfType<PhoneNumber>().First().Print();
            } catch { }
            try {
                contactDatas.OfType<PostalAddress>().First().Print();
            } catch { }
        }
    }
}
