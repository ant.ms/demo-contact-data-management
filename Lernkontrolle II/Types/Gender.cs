﻿namespace Lernkontrolle_II {
    enum Gender {
        Male,
        Female,
        NonBinary,
        Other
    }
}